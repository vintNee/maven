# maven
My personal maven repository.

## Usage
pom.xml:
```xml
<repositories>
	<repository>
		<id>com.onlyxiahui</id>
		<url>https://gitee.com/onlyxiahui/maven/raw/master/repository</url>
	</repository>
</repositories>
```
